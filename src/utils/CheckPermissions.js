import { PermissionsAndroid } from 'react-native';

export const checkCameraPermission = async () => {
    try {
        const check = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.CAMERA);
        return check;
    }catch (e) {
        console.log("error", e.message);
    }
}

export const checkWriteExternalStorage = async () => {
    try {
        const check = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE);
        return check;
    } catch (e) {
        console.log("error", e.message);
    }
}