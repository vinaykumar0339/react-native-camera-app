import { PermissionsAndroid } from 'react-native';

export const requestCameraPermission = async () => {
    try {
        const request = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.CAMERA);
        return request;
    }catch (e) {
        console.log("error", e.message);
    }
}

export const requestWriteExternalStorage = async () => {
    try {
        const request = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE);
        return request;
    } catch (e) {
        console.log("error", e.message);
    }
}