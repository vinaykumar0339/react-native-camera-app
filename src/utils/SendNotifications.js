import PushNotification from 'react-native-push-notification';




export const localNotification = (channelId, title, messagae) => {

    PushNotification.channelExists(channelId, function (exists) {
        if (exists) {
            PushNotification.localNotification({
                channelId: channelId,
                title: title,
                message: messagae,
            })
        } else {
            PushNotification.createChannel(
                {
                  channelId: channelId, // (required)
                  channelName: "Sending Notifications", // (required)
                  channelDescription: "Sending notifications After taking picture", // (optional) default: undefined.
                  playSound: true, // (optional) default: true
                  soundName: "default", // (optional) See `soundName` parameter of `localNotification` function
                  importance: 4, // (optional) default: 4. Int value of the Android notification importance
                  vibrate: true, // (optional) default: true. Creates the default vibration patten if true.
                },
                (created) => {
                    if(created) {
                        PushNotification.localNotification({
                            channelId: channelId,
                            title: title,
                            message: messagae,
                        })
                    }
                }
              );
        }
      });
}