import React, { Component } from 'react'
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import { RNCamera } from 'react-native-camera';
import RNFetchBlob from 'rn-fetch-blob';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { checkCameraPermission, checkWriteExternalStorage } from '../utils/CheckPermissions';
import { requestCameraPermission, requestWriteExternalStorage } from '../utils/RequestPermissions';
import { localNotification } from '../utils/SendNotifications';

export default class Camera extends Component {

    constructor() {
        super();
        this.state = {
            back: true,
        }
    }

    savePicture = async () => {
        if (this.camera) {
            const options = {
                quality: 0.5,
                base64: true,
            };
            const data = await this.camera.takePictureAsync(options);
            const pictureName = data.uri.split('/')[data.uri.split('/').length - 1]
            const folder = `/storage/emulated/0/camera_app`;
            const path = `${folder}/${pictureName}`
            const exists = await RNFetchBlob.fs.exists(folder)

            if (exists) {
                await RNFetchBlob.fs.writeFile(path, data.base64, 'base64')
                await RNFetchBlob.fs.scanFile([{ path: path }]);

                // sending notificaiton
                localNotification("sendCapture", "Image Saved", "You taken the picture")
            } else {
                await RNFetchBlob.fs.mkdir(folder);
                await RNFetchBlob.fs.writeFile(path, data.base64, 'base64')
                await RNFetchBlob.fs.scanFile([{ path: path }]);
                localNotification("sendCapture", "Image Saved", "You taken the picture")
            }

        }
    }

    takePicture = async () => {
        try {
            const cameraPermission = await checkCameraPermission();
            if (cameraPermission) {
                const writePermission = await checkWriteExternalStorage();
                if (writePermission) {
                    await this.savePicture();
                } else {
                    await requestWriteExternalStorage();
                }

            } else {
                await requestCameraPermission();
            }

        } catch (e) {
            console.log("error" + e.message);
        }
    };

    rotateCamera = () => {
        this.setState({
            back: !this.state.back
        })
    }

    render() {
        return (
            <View style={styles.container}>
                <RNCamera
                    ref={ref => {
                        this.camera = ref;
                    }}
                    style={styles.cameraScreen}
                    type={this.state.back ? RNCamera.Constants.Type.back : RNCamera.Constants.Type.front}
                    flashMode={RNCamera.Constants.FlashMode.on}
                    playSoundOnCapture={true}
                    captureAudio={false}
                >
                    <View style={styles.buttons}>
                        <TouchableOpacity onPress={this.takePicture} >
                            <Icon name="camera" size={60} color="#fff" />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.rotateCamera} >
                            <Icon name="flip-camera-ios" size={60} color="#fff" />
                        </TouchableOpacity>
                    </View>
                </RNCamera>

            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'black',
    },
    cameraScreen: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    buttons: {
        flexDirection: 'row',
        width: "100%",
        justifyContent: 'space-evenly',
        alignItems: 'center',
        padding: 20,
    },
});